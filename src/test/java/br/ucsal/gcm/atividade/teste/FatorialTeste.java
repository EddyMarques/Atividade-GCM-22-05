package br.ucsal.gcm.atividade.teste;

import org.junit.Test;

import br.ucsal.gmc.atividade.Fatorial;
import junit.framework.Assert;

public class FatorialTeste {
	
	@SuppressWarnings("deprecation")
	@Test
	public void CalcularFatorial5(){
		long fat = 5;
		
		Assert.assertEquals(120, Fatorial.fatorial(fat));
	}

}
